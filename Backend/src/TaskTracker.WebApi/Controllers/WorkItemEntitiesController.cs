﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TaskTracker.WebApi.Data;
using TaskTracker.WebApi.Models;

namespace TaskTracker.WebApi.Controllers
{
    [Authorize(Policy = "ApiUser")]
    [Route("api/[controller]")]
    [ApiController]
    public class WorkItemsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public WorkItemsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/workitems
        [HttpGet]
        public async Task<ActionResult<IEnumerable<WorkItemEntity>>> GetWorkItems()
        {
            return await _context.WorkItems.ToListAsync();
        }

        // GET: api/workitems/5
        [HttpGet("{id}")]
        public async Task<ActionResult<WorkItemEntity>> GetWorkItemEntity(int id)
        {
            var workItemEntity = await _context.WorkItems.FindAsync(id);

            if (workItemEntity == null)
            {
                return NotFound();
            }

            return workItemEntity;
        }

        // PUT: api/workitems/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutWorkItemEntity(int id, WorkItemEntity workItemEntity)
        {
            if (id != workItemEntity.Id)
            {
                return BadRequest();
            }

            _context.Entry(workItemEntity).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!WorkItemEntityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/workitems
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<WorkItemEntity>> PostWorkItemEntity(WorkItemEntity workItemEntity)
        {
            _context.WorkItems.Add(workItemEntity);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetWorkItemEntity", new { id = workItemEntity.Id }, workItemEntity);
        }

        // DELETE: api/workitems/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<WorkItemEntity>> DeleteWorkItemEntity(int id)
        {
            var workItemEntity = await _context.WorkItems.FindAsync(id);
            if (workItemEntity == null)
            {
                return NotFound();
            }

            _context.WorkItems.Remove(workItemEntity);
            await _context.SaveChangesAsync();

            return workItemEntity;
        }

        private bool WorkItemEntityExists(int id)
        {
            return _context.WorkItems.Any(e => e.Id == id);
        }
    }
}
