﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using TaskTracker.WebApi.Models;

namespace TaskTracker.WebApi.Data
{
    public class ApplicationDbContext : IdentityDbContext<AppUser>
    {
        public ApplicationDbContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<WorkItemEntity> WorkItems { get; set; }
        public DbSet<TaskTrackingEntity> TaskTrackings { get; set; }
    }
}
