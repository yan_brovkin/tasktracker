﻿namespace TaskTracker.WebApi.Contracts
{
    public class CredentialsContract
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
