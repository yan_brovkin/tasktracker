﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TaskTracker.WebApi.Migrations
{
    public partial class Add_TaskTracking : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<TimeSpan>(
                name: "ActualExecutionTime",
                table: "WorkItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AppUserId",
                table: "WorkItems",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "TaskTrackings",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    WorkItemEntityId = table.Column<int>(nullable: false),
                    From = table.Column<TimeSpan>(nullable: true),
                    Till = table.Column<TimeSpan>(nullable: true),
                    TimeLeft = table.Column<TimeSpan>(nullable: true),
                    Comment = table.Column<string>(maxLength: 50, nullable: true),
                    AppUserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaskTrackings", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TaskTrackings");

            migrationBuilder.DropColumn(
                name: "ActualExecutionTime",
                table: "WorkItems");

            migrationBuilder.DropColumn(
                name: "AppUserId",
                table: "WorkItems");
        }
    }
}
