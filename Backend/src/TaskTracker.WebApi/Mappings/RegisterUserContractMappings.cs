﻿using AutoMapper;
using TaskTracker.WebApi.Contracts;
using TaskTracker.WebApi.Models;

namespace TaskTracker.WebApi.Mappings
{
    public class RegisterUserContractMappingsProfile: Profile
    {
        public RegisterUserContractMappingsProfile()
        {
            CreateMap<RegisterUserContract, AppUser>()
                .ForMember(au => au.UserName, map => map.MapFrom(vm => vm.Email));
        }
    }
}
