﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TaskTracker.WebApi.Models
{
    public class TaskTrackingEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        public int WorkItemEntityId { get; set; }
        public TimeSpan? From { get; set; }
        public TimeSpan? Till { get; set; }
        public TimeSpan? TimeLeft { get; set; }
        [StringLength(50)]
        public string Comment { get; set; }
        public string AppUserId { get; set; }
    }
}
