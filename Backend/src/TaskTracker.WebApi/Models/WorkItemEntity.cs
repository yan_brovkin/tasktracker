﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TaskTracker.WebApi.Models
{
    public class WorkItemEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        [StringLength(50)]
        public string Title { get; set; }
        [StringLength(20)]
        public string Type { get; set; }
        public int Priority { get; set; }
        public int PlannedExecutionTime { get; set; }
        public string AppUserId { get; set; }
        public TimeSpan? ActualExecutionTime { get; set; }

    }
}
