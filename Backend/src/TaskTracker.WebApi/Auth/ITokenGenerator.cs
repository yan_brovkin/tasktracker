﻿using System.Security.Claims;
using System.Threading.Tasks;

namespace TaskTracker.WebApi.Auth
{
    public interface ITokenGenerator
    {
        Task<string> GenerateJwt(ClaimsIdentity identity, string userName);
    }
}
