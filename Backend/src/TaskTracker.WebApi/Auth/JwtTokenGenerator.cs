﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using TaskTracker.WebApi.Models;

namespace TaskTracker.WebApi.Auth
{
    public class JwtTokenGenerator : ITokenGenerator
    {
        private readonly IJwtFactory _jwtFactory;
        private readonly JsonSerializerSettings _serializerSettings;
        private readonly IOptions<JwtIssuerOptions> _jwtOptions;

        public JwtTokenGenerator(IJwtFactory jwtFactory, IOptions<JwtIssuerOptions> jwtOptions)
        {
            _jwtFactory = jwtFactory;
            _jwtOptions = jwtOptions;
            _serializerSettings = new JsonSerializerSettings { Formatting = Formatting.Indented };
        }

        public async Task<string> GenerateJwt(ClaimsIdentity identity, string userName)
        {
            var response = new
            {
                id = identity.Claims.Single(c => c.Type == "id").Value,
                auth_token = await _jwtFactory.GenerateEncodedToken(userName, identity),
                expires_in = (int)_jwtOptions.Value.ValidFor.TotalSeconds
            };

            return JsonConvert.SerializeObject(response, _serializerSettings);
        }
    }
}
