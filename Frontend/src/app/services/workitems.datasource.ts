import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {Observable, BehaviorSubject, of, empty} from "rxjs";
import {WorkItemService} from "./work-item.service";
import {catchError, finalize} from "rxjs/operators";
import { workItem } from '../contracts';



export class WorkitemsDataSource implements DataSource<workItem> {

    private workitemsSubject = new BehaviorSubject<workItem[]>([]);

    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    constructor(private workitemService: WorkItemService) {

    }

    loadWorkitems(id:number,
                filter:string,
                sortDirection:string,
                pageIndex:number,
                pageSize:number) {

        this.loadingSubject.next(true);

        this.workitemService.getAll()
        .pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubject.next(false))
            )
        .subscribe(workitems => this.workitemsSubject.next(workitems));

    }

    connect(collectionViewer: CollectionViewer): Observable<workItem[]> {
        console.log("Connecting data source");
        return this.workitemsSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.workitemsSubject.complete();
        this.loadingSubject.complete();
    }

    addItem(item: workItem): void {
        const workItems = this.workitemsSubject.getValue();
        workItems.push(item);
        this.workitemsSubject.next(workItems);
    }

    removeItem(item: workItem): void {
        const workItems = this.workitemsSubject.getValue().filter(i => i.id !== item.id );
        this.workitemsSubject.next(workItems);
    }

    getLength(): number {
        return this.workitemsSubject.getValue().length;
    }
}