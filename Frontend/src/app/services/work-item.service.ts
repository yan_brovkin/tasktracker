import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '@env/environment';
import { workItem } from '../contracts';
import { Observable } from 'rxjs';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({ providedIn: 'root' })
export class WorkItemService { 
  constructor(private http: HttpClient){

  }

  getAll() {
        return this.http.get<any[]>(`${environment.apiUrl}/api/workitems`);
  }

  delete(id): Observable<workItem> {
    return this.http.delete<workItem>(`${environment.apiUrl}/api/workitems/${id}`);
  }

  getDetails(id) {
    return this.http.get<any>(`${environment.apiUrl}/api/workitems/${id}`);
  }

  create(item: workItem): Observable<workItem> {
    return this.http.post<workItem>(`${environment.apiUrl}/api/workitems`, item, httpOptions);
  }
}
