import { NgModule }  from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './helpers';
import { RegisterComponent } from './register/register.component';

const routes: Routes = [
  { path: 'workitem', loadChildren: () => import('./work-item/work-item.module').then(mod => mod.WorkItemModule) },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path : '', loadChildren: () => import('./work-item/work-item.module').then(mod => mod.WorkItemModule), canActivate: [AuthGuard] },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: [
  ]
})

export class AppRoutingModule { }