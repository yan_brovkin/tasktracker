export class workItem
{
    id: number;
    title: string;
    type: string;
    priority: number;
    plannedExecutionTime: number;
    constructor() {
        this.id = 0;
        this.title = '';
        this.type = '';
        this.priority = 0;
        this.plannedExecutionTime = 0;
    }
}