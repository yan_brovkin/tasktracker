import { Component, OnInit, Input } from '@angular/core';
import { workItem } from '../../contracts';
import { ActivatedRoute } from '@angular/router';
import {SelectionModel} from '@angular/cdk/collections';
import { WorkItemService, WorkitemsDataSource } from 'src/app/services';
import { Observable } from 'rxjs';
import { finalize, take } from 'rxjs/operators';

@Component({
  selector: 'app-work-item-detail',
  templateUrl: './work-item-detail.component.html',
  styleUrls: ['./work-item-detail.component.scss']
})
export class WorkItemDetailComponent implements OnInit {
  row: workItem;
  constructor(
    private route: ActivatedRoute,
    private workitemService: WorkItemService) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      let id = Number(params.get('id'));
      this.workitemService
      .getDetails(Number(id))
      .pipe(take(1))
      .subscribe((item: workItem) => {
        this.row = item;
        debugger;
      });
    });
  }

}
