import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { workItem } from 'src/app/contracts';

@Component({
  selector: 'app-add-work-item',
  templateUrl: './add-work-item.component.html',
  styleUrls: ['./add-work-item.component.scss']
})
export class AddWorkItemComponent implements OnInit {

  data: workItem;

  constructor(public dialogRef: MatDialogRef<AddWorkItemComponent>) {}

  ngOnInit(): void {
    this.data = new workItem();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
