import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { SelectionModel, CollectionViewer } from '@angular/cdk/collections';
import { AuthenticationService, WorkitemsDataSource, WorkItemService } from '../services';
import { take, tap } from 'rxjs/operators';
import { workItem } from '../contracts';
import { Router } from '@angular/router';
import { MatDatepickerInputEvent, MatDialog, MatSnackBar, MatProgressSpinner  } from '@angular/material';
import { AddWorkItemComponent } from './add-work-item/add-work-item.component';

@Component({
  selector: 'app-work-item-list',
  templateUrl: './work-item-list.component.html',
  styleUrls: ['./work-item-list.component.scss']
})
export class WorkItemListComponent implements OnInit {
  private dataSource: WorkitemsDataSource;
  private selection: SelectionModel<workItem>;
  private currentUser: any;
  private isBusy: boolean;
  private displayedColumns: string[];
  private selectedRow: workItem;
  private dateFormControl: FormControl;

  constructor(
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private authenticationService: AuthenticationService,
    private workitemService: WorkItemService,
    private router: Router) 
  { 
    
  }

  ngOnInit() {
    this.isBusy = false;
    this.dateFormControl = new FormControl(new Date());
    this.displayedColumns = ['select','title', 'type', 'priority', 'plannedExecutionTime'];
    this.dataSource = new WorkitemsDataSource(this.workitemService);
    this.selection = new SelectionModel<workItem>(true, []);
    this.dataSource.loadWorkitems(1, '', 'asc', 0, 3);
  }

  addWorkItem(): void {
    const dialogRef = this.dialog.open(AddWorkItemComponent, {
      width: '250px'
    });

    dialogRef.afterClosed().subscribe((result: workItem) => {
      this.workitemService.create({...result, priority: Number(result.priority), plannedExecutionTime: Number(result.plannedExecutionTime)})
      .pipe(take(1))
      .subscribe(result => {
        this.dataSource.addItem(result);
        this.showInfo(`task '${result.title}' added`);
      },
      error=> console.log(error))
    });
  }

  deleteWorkItems(): void {
    if (!this.selection.hasValue())
      return;
    
    this.isBusy = true;
    for (const item of this.selection.selected)
    {
      this.workitemService.delete(item.id)
      .pipe(take(1))
      .subscribe(result => { 
        this.dataSource.removeItem(result);
        this.selection.toggle(item);
      });
    }
    this.isBusy = false;
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.getLength();
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.connect(<CollectionViewer> {})
        .pipe(
          take(1),
          tap(w => w.forEach(row => this.selection.select(row))))
        .subscribe();
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: workItem): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }

  showInfo(message: string) {
    this.snackBar.open(message, null, {
      duration: 2000,
    });
  }

  onSelectWorkItem(row: workItem){
    console.log(row);
    this.selectedRow = row;
    this.router.navigate(['/workitem', row.id]);//{ replaceUrl: true });
  }

  dateChanged(event: MatDatepickerInputEvent<Date>) {
    //TODO: search task items that are created on selected date and put it onto the table
    debugger;
  }
}
