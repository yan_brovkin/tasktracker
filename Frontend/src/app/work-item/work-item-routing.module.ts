import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WorkItemListComponent } from './work-item-list.component';
import { WorkItemDetailComponent } from './work-item-detail/work-item-detail.component';


const routes: Routes = [
  { path: '', component: WorkItemListComponent
  },

  { path: 'workitem/:id', component: WorkItemDetailComponent },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule, 
  ]
})
export class WorkItemRoutingModule { }
