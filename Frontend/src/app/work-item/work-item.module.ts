import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WorkItemRoutingModule } from './work-item-routing.module';
import { WorkItemListComponent } from './work-item-list.component';
import { 
  MatTableModule, 
  MatDatepickerModule, 
  MatNativeDateModule, 
  MatFormFieldModule,
  MatButtonModule, 
  MatCardModule, 
  MatProgressSpinnerModule,
  MAT_DATE_FORMATS, 
  MAT_DATE_LOCALE,
  MatInputModule,
  MatIconModule,
  MatCheckboxModule,
  MatBadgeModule,
  MatSnackBarModule
} from '@angular/material';
import { MatDialogModule, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { WorkItemDetailComponent } from './work-item-detail/work-item-detail.component';
import { AddWorkItemComponent } from './add-work-item/add-work-item.component';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MM YYYY',
    dateA11yLabel: 'DD/MM/YYYY',
    monthYearA11yLabel: 'MM YYYY',
  },
};

@NgModule({
  declarations: [WorkItemListComponent, WorkItemDetailComponent, AddWorkItemComponent ],
  imports: [
    CommonModule,
    WorkItemRoutingModule,
    MatTableModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatDialogModule,
    MatCheckboxModule,
    MatBadgeModule,
    MatSnackBarModule,
    FormsModule
  ],
  exports: [
    MatTableModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule
  ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' }, 
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
  ],
  entryComponents: [ AddWorkItemComponent ]
})
export class WorkItemModule { }
